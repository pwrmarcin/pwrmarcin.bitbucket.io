function initMap() {
	var uluru = { lat: 50.772547, lng: 16.267515 };
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 18,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
}
function trackEvent(sender){
  var val = $(sender).data('label');
	gtag('event', 'Click', {
	  'event_category': 'MainLinki',
	  'event_label': 'Awareness'+val
	});
}

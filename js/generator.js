$(document).ready(function() {
	initGen();
});

function initGen(){
	$("#drop").dropzone({ url: "/file/post" });
	bindBox();
}

function addPic(){
	var nextIdx=$('.genImageBox').length;
	var data=nextIdx;
	var template = '<div id="drop{{.}}" class="genImageBox"><div class="img-gen-title">Upuść obraz tutaj</div><div class="c0"><div class="circle"><div class="circle-text">9,99 zł</div></div></div></div>';
	var html = Mustache.to_html(template, data);
	$('.generator').append(html);
	$("#drop"+nextIdx).dropzone({ url: "/file/post" });
	bindBox();
}
function bindBox(){
$( ".genImageBox" ).hover(function() {
		$( this ).css("border","3px dotted gray");
	  }, function() {
		$( this ).css("border","0px");
	  }
	);
	$(".img-gen-title").click(function(){
		var text=$(this).html();
		if(!text.startsWith('<')){
			$(this).html('<input autofocus type="text" class="input-gen" value="'+text+'">');
			$(this).find("input").focus().setCursorPosition(text.length);
			focusOutBind($(this));
		}
	});
	$(".circle-text").click(function(){
		insertInput(this);
	});
}
function insertInput(sender){
	var text=$(sender).html();
		if(!text.startsWith('<')){
			$(sender).html('<input autofocus type="text" class="input-gen" value="'+text+'">');
			$(sender).find("input").focus().setCursorPosition(text.length);
			focusOutBind(sender);
		}
}
function focusOutBind(sender){	
	$(sender).find("input").focusout(function(){
		var text=$(this).val();
		if(text.length){		
			$(sender).html(text);
		}
	});
}
$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};
function copyToClipboard(){
		var value = $(".generator").html(); 
        var $temp = $("<input>");
          $("body").append($temp);
          $temp.val(value).select();
          document.execCommand("copy");
          $temp.remove();
}
var allArticles;
var basketBodyTether;
var fullyLoaded=false;
var lastSearchLength=0;
var page;
var t1;
var currentData;
function initMap() {
	var uluru = { lat: 50.772547, lng: 16.267515 };
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 18,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
}
$(document).ready(function() {
  initCookie();
  initAlert();
  initLoad();
	initSearchbox();
  initBasket();
});
function initCookie(){
  if(Cookies.get('Zamowienie')===undefined){
    var zamowienie=[];
    Cookies.set('Zamowienie', zamowienie, { expires: 14 });
  }
}
function initAlert(){
  if(Cookies.get('Alert')===undefined){
    showAlert();
  }
}
function initLoad(){
  if(allArticles === undefined || allArticles === null){
    var postData = { page: "0", pageSize: "100" }
    showLoading('.lista-artykulow');
    $.post("http://35.189.206.41/Home/AsortymentNaStanie", postData, function( data ) {
      allArticles=data;
      allArticles.Rows=toAssociativeArray(allArticles.Rows);
      loadList();
      hideLoading();
      initFullLoad();
    }, "json");
  }}
function initSearchbox(){
  $("#search").keyup(function(){
      loadList();
      riseKeypressEvent();
  });
}
function initBasket(){
    $('.basketBody').show();
      basketBodyTether=new Tether({
      element: '.basketBody',
      target: '.search',
      attachment: 'top right',
      targetAttachment: 'top right'
    });
    updateBasket();
    basketBodyTether.position();
}
function initFullLoad(){
  $('.db-load-progress').show();
  var postData = { page: "0", pageSize: "3500" };
      setTimeout(progress, 1000);
    $.post("http://35.189.206.41/Home/AsortymentNaStanie", postData, function( data ) {
      allArticles=data;
      allArticles.Rows=toAssociativeArray(allArticles.Rows);
      fullyLoaded=true;
      $('.db-load-progress-bar').css("width","100%");
      $('.db-load-progress-bar').text('Ładowanie:100%');
      $('.db-load-progress-bar').addClass("bg-success");
      $('.db-load-progress').fadeOut();
      initCategoryFilter();
      loadList();
    }, "json");
}
function initCategoryFilter(){
  var cat=[];
  var catArr=[];
  for (var i in allArticles.Rows){
    if(cat[allArticles.Rows[i].Cels[2].Value]===undefined){
      cat[allArticles.Rows[i].Cels[2].Value]="";
      catArr.push(allArticles.Rows[i].Cels[2].Value);
    }
  }
  catArr.sort();
  buildFilter(catArr);
  $('.filter-row').fadeIn("slow", function() {
    basketBodyTether.position();
  });
}
function buildFilter(cat){
  var template = "<a href=\"#\" class=\"filter-item filter-clear badge badge-danger\">Wyczyść</a>{{#.}}<a href=\"#\" class=\"filter-item badge badge-light\">{{.}}</a>{{/.}}";
  var html = $(Mustache.to_html(template, cat));
  $('.filter-items').html(html);
  bindFilters();
}
function bindFilters(){
  $('.filter-item').click(toggleFilter);
}
function toggleFilter(e){
  stopPropagation(e);
  if($(this).hasClass("filter-clear")){
    $('.filter-item.badge-dark').removeClass('badge-dark').addClass('badge-light');
  }else{
    if($(this).hasClass("badge-light")){
      $('#search').val('');
      riseFilterEvent(this);
      $(this).removeClass('badge-light').addClass('badge-dark');
    }else{
      $(this).removeClass('badge-dark').addClass('badge-light')
    }
  }
  loadList();
}
function filter(currentCat){
  var result = false;
  if($('.filter-item.badge-dark').length==0) return true;
  $('.filter-item.badge-dark').each(function(){
    if(this.innerText==currentCat){
      result = true;
      return false;
    }
  });
  return result;
}
function showAlert(){
  $('.alertStart').show();
}
function closeAlertStart(e){
  stopPropagation(e);
  $('.alertStart').hide();
  var alert=true;
  Cookies.set('Alert', alert, { expires: 1 });
  return false;
}
function stopPropagation(e){
  e = e || window.event;
  if (e.stopPropagation) {    // standard
        e.stopPropagation();
    } else {    // IE6-8
        e.cancelBubble = true;
  }
  if(e.preventDefault) e.preventDefault();
}
function progress(){
  var w = parseInt($('.db-load-progress-bar').attr("aria-valuenow"));
  if(!fullyLoaded && w < 99){
    w+=25;
    if(w==100){
      w=99;
    }
    $('.db-load-progress-bar').css("width",w+"%");
    $('.db-load-progress-bar').attr("aria-valuenow", w);
    $('.db-load-progress-bar').text('Ładowanie:'+w+'%');
    setTimeout(progress, 500);
  }
}
function loadList(){
  var displayTable={
    Name:allArticles.Name,
    Header:allArticles.Header,
    Rows:[]
  };
  var search=$("#search").val().toUpperCase();
  for(var key in allArticles.Rows){
    var self=allArticles.Rows[key];
    if((self.Cels[0].Value.toUpperCase().includes(search.toUpperCase()) || self.Cels[1].Value.toUpperCase().includes(search.toUpperCase())|| self.Cels[2].Value.toUpperCase().includes(search.toUpperCase())) && filter(self.Cels[2].Value)){
      displayTable.Rows.push(self);
    }
  }
  if(displayTable.Rows.length>0){
    page=0;
    populateList(displayTable);
  }
}
function toAssociativeArray(_Object){
  var _Array = new Array();
  for(var idx in _Object){
    var kod = _Object[idx].Cels[0].Value;
    _Array[kod] = _Object[idx];
  }
  return _Array;
}
function showLoading(className){
  t1 = new Date();
  $('.loading').show();
  new Tether({
      element: '.loading',
      target: className,
      attachment: 'middle center',
      targetAttachment: 'middle center',
      offset: '-64px 0'
    });
}
function hideLoading(){
  var t2 = new Date();
  var dif = (t2.getTime() - t1.getTime())/1000;
  console.log('loading time:'+dif+'s');
  $('.loading').hide();
}
function loadMore(e){
  stopPropagation(e);
  populateList(currentData);
  trackEvent(e.target);
}
function populateList(data){
  var pageSize=100;
  if(data.Rows && page == 0){
    console.log(data.Rows.length);
    lastSearchLength=data.Rows.length;
    data.Rows=data.Rows.sort(compare);
    currentData=data;
  }
  if(page==0){
    var template = "<table class=\"table-art\">" +
            "<thead><tr>{{#Header}}<th class=\"display-{{Display}} type-{{Type}}\">{{DisplayName}}</th>{{/Header}}<th></th></tr></thead>" +
            "<tbody>{{#Rows}}{{#.}}<tr>{{#Cels}}<td>{{Value}}</td>{{/Cels}}<td class=\"d-flex td-add justify-content-between\"><div class=\"button-art\">-</div><div>0</div><div class=\"button-art\">+</div></td></tr>{{/.}}{{/Rows}}</tbody>" +
            "</table>";
    var dataPage=takePage(pageSize);
    var html = $(Mustache.to_html(template, dataPage));
    updateFromCookie(html);
    $('.lista-artykulow').html(html);
  }else{
    showLoading('.lista-artykulow');
    var templateShort = "{{#Rows}}{{#.}}<tr>{{#Cels}}<td>{{Value}}</td>{{/Cels}}<td class=\"d-flex td-add justify-content-between\"><div class=\"button-art\">-</div><div>0</div><div class=\"button-art\">+</div></td></tr>{{/.}}{{/Rows}}";
    var dataPage=takePage(pageSize);
    var html = $(Mustache.to_html(templateShort, dataPage));
    updateFromCookie(html);
    $('.table-art tbody').append(html);
    hideLoading();
  }
  bindButtonArt();
}
function takePage(pageSize){
  var resultData = $.extend(true, {}, currentData); ;
  var rows=[];
  var start=page*pageSize;
  if(currentData.Rows.length < start+pageSize){
    pageSize=currentData.Rows.length-start;
    if(pageSize<0){
        pageSize=0;
      }
  }
  var end=start+pageSize;
  for(i=start;i<currentData.Rows.length && i<end;i++){
    rows.push(currentData.Rows[i]);
  }
  resultData.Rows=rows;
  if(currentData.Rows.length<=start+pageSize){
    $('.pager').hide();
  }else{
    $('.pager').show();
  }
  page++;
  pagerProgress(start+pageSize);
  return resultData;
}
function pagerProgress(items){
  var max=currentData.Rows.length;
  var text ="Przeglądanie "+items+" z "+max;
  $('.pager-progress-bar').attr('aria-valuemax',max);
  $('.pager-progress-bar').attr('aria-valuenow',items);
  $('.pager-progress-bar').text(text);
  var width=(items/max)*100;
  $('.pager-progress-bar').css("width",width+"%");
}
function updateFromCookie(html){
  $.each(html.find('tr'),function(index, value){
    if($(value).find('td').length>0){
      var kod=$(value).find('td')[0].innerText;
      var count=$(value).find('.td-add').children()[1].innerText;
      $(value).find('.td-add').children()[1].innerText=findCountInCookie(kod, count);
    }
  });
}
function findCountInCookie(kod, count){
  var zamowienie=Cookies.getJSON('Zamowienie');
  for (var i = 0; i < zamowienie.length; i++){
    if(zamowienie[i].kod==kod){
      count = zamowienie[i].count;
      break;
    }
  }
  return count;
}
function bindButtonArt(){

  $('.button-art').unbind().click(operation);
}
function operation(event){
  var operator=$(this).html();
  var value=0;
  if(operator=="-"){
    value=$(this).next().html();
    value=subArt(this, value);
    $(this).next().html(value);
  }else{
    value=$(this).prev().html();
    value=addArt(this, value);
    $(this).prev().html(value);
  }
}
function subArt(sender, count){
  var tr=$(sender).parent().parent();
  var cookieElement=formatRow(tr, count.replace(/,/, '.'));
  if(cookieElement.miara=="KG"){
    count=parseFloat(count)-parseFloat(cookieElement.opak);
  }else{
    count=parseFloat(count)-1;
  }
  if(count<0){
    count=0;
  }
  cookieElement.count=count;
  editCookieElement(cookieElement);
  return count;
}
function addArt(sender, count){
  var tr=$(sender).parent().parent();
  var cookieElement=formatRow(tr, count.replace(/,/, '.'));
  if(cookieElement.miara=="KG"){
    count=parseFloat(count)+parseFloat(cookieElement.opak);
  }else{
    count=parseFloat(count)+1;
  }
  cookieElement.count=count;
  editCookieElement(cookieElement);
  return count;
}
function formatRow(tr, count){
  var kod;
  var name;
  var miara;
  var opak;
  var price;
  kod = tr.find('td')[0].innerText;
  name=allArticles.Rows[kod].Cels[1].Value;
  miara=allArticles.Rows[kod].Cels[3].Value;
  opak=allArticles.Rows[kod].Cels[4].Value;
  price=allArticles.Rows[kod].Cels[6].Value.replace(/,/, '.');
  if(opak==""){
    opak=1;
  }
  return {kod:kod,name:name,miara:miara,opak:opak,price:price, count:count};
}
function editCookieElement(cookieElement){
  var zamowienie=Cookies.getJSON('Zamowienie');

  var exists=0;
  for (var i = 0; i < zamowienie.length; i++){
    if(zamowienie[i].kod==cookieElement.kod){
      zamowienie[i].count=cookieElement.count;
      if(zamowienie[i].count==0){
        zamowienie.splice(i,1);
      }
      exists=1;
      break;
    }
  }
  if(exists==0 && cookieElement.count > 0){
    zamowienie.push(cookieElement);
  }
  Cookies.set('Zamowienie', zamowienie, { expires: 14 });
  updateBasket();
  raiseBasketEvent(cookieElement.kod, cookieElement.count);
}
function updateBasket(){
  var zamowienie=Cookies.getJSON('Zamowienie');
  var itemCount=zamowienie.length;
  var sum=0;
  for (var i = 0; i < zamowienie.length; i++){
    sum+=parseFloat(zamowienie[i].count)*parseFloat(zamowienie[i].price);
  }
  $('.basket-count').text(itemCount);
  sum=Math.round(sum * 100) / 100;
  $('.basket-sum').text(sum.toFixed(2));
  updateBasketSummary();
}
function toggleBasket(){
  if($('.basket').css("overflow")=="hidden"){
    var zamowienie = Cookies.getJSON('Zamowienie')
    if(zamowienie !== undefined && zamowienie.length > 0){
      $('.basket-icon').hide();
      $('.basket-icon-up').show();
      $('.basket').css({overflow:"auto",width:"auto",height:"auto"});
      $('.top-basket').css("width","auto");
      $('.basket-expand').hide();
      $('.basket-colapse').show();
      updateBasketSummary();
    }
  }else{
    closeBasket();
  }
  basketBodyTether.position();
}
function closeBasket(){
    $('.basket-icon').show();
    $('.basket-icon-up').hide();
    $('.basket').css({overflow:"hidden",width:"146px",height:"59px"});
    $('.top-basket').css("width","146px");
    $('.basket-colapse').hide();
    $('.basket-expand').show();
    loadList();
}
function updateBasketSummary(){
  if($('.basket').css("overflow")=="auto"){
    var template = "<table class=\"table-art-basket\">" +
            "<thead><tr><th>Kod</th><th>Nazwa towaru</th><th>Miara</th><th>Pakowany</th><th>Cena z VAT</th><th></th></tr></thead>" +
          "<tbody>{{#zamowienie}}<tr><td>{{kod}}</td><td>{{name}}</td><td>{{miara}}</td><td>{{opak}}</td><td>{{price}}</td><td class=\"d-flex td-add justify-content-between\"><div class=\"button-art-basket\">-</div><div>{{count}}</div><div class=\"button-art-basket\">+</div></td></tr>{{/zamowienie}}</tbody>" +
          "</table>";
    var zamowienie='{"zamowienie":'+Cookies.get('Zamowienie')+'}';
    var html = $(Mustache.to_html(template, JSON.parse(zamowienie)));
    $('.basket-copy').html(html);
    bindButtonArtBasket();
  }
}
function bindButtonArtBasket(){
  $('.button-art-basket').unbind().click(operation);
}
function clearBasket(){
  var zamowienie=[];
  Cookies.set('Zamowienie', zamowienie, { expires: 14 });
  closeBasket();
  updateBasket();
  $('.checkout-box').hide();
  basketBodyTether.position();
}
function confirmBasket(){
  var link=generateLink();
  if($('.checkout-mail').attr('href').endsWith("body=")){
    var fullEmailLink=$('.checkout-mail').attr('href')+link;
    $('.checkout-mail').attr('href',fullEmailLink);
    $('.checkout-copy').attr('href',link);
  }
  $('.checkout-box').show();
}
function closeCheckout(){
  $(".success-icon").css('visibility','hidden');
  $('.checkout-box').hide();
}
function copyCheckout(e){
  //stopPropagation(e);
  var value = generateLink();
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val(value).select();
  document.execCommand("copy");
  $temp.remove();
  $(".success-icon").css('visibility','visible');
  //$(e.target).append('<div class="success-icon"></div>');
  riseCopyEvent();
}
function generateLink(){
  var zamowienie = Cookies.getJSON('Zamowienie');
  var link="http://"+window.location.host+"/zamowienie.html?v=";
  for (i=0;i<zamowienie.length;i++){
    link+=zamowienie[i].kod+"_"+zamowienie[i].count+"!";
  }
  return link;
}
function compare(a, b) {
   if (a.Cels[1].Value < b.Cels[1].Value)
      return -1
   if (a.Cels[1].Value > b.Cels[1].Value)
      return 1
   return 0
}
function riseKeypressEvent(){
gtag('event', 'Keypress', {
		  'event_category': 'Search',
		  'event_label': $("#search").val(),
		  'value': lastSearchLength
		});
}
function raiseBasketEvent(kod,count){
  gtag('event', 'Operation', {
		  'event_category': 'Basket',
		  'event_label': kod,
		  'value': count
		});
}
function raiseLeaveCheckoutEvent(sum){
  gtag('event', 'Leave', {
		  'event_category': 'Checkout',
		  'event_label': sum,
		  'value': sum
		});
}
function riseCopyEvent(){
  var count = parseInt($('.basket-count').text());
  var sum = parseInt($('.basket-sum').text());
  gtag('event', 'Copy', {
		  'event_category': 'Checkout',
		  'event_label': sum+'x'+count,
		  'value': sum
		});
}
function trackEvent(sender){
  var val = $(sender).data('label');
	gtag('event', 'Click', {
	  'event_category': 'AsortLinki',
	  'event_label': 'Awareness'+val
	});
}
function riseFilterEvent(sender){
  var val = $(sender).text();
	gtag('event', 'Click', {
	  'event_category': 'AsortFilter',
	  'event_label': val
	});
}

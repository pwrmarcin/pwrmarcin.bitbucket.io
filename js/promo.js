loadPromo();
$(document).ready(function() {
  bindPromoSearch();
});

function loadPromo(){
  var tempHtml=$(".naj-oferty-carousel").load("promo.html");
}
function bindPromoSearch(){
  if($('.carousel-item').length==0){
    setTimeout(bindPromoSearch, 250);
  }else{
    $('.carousel-item').click(function(){
      trackEvent(this);
      var searchFor=$(this).find('div.promo-search');
      if(searchFor.length > 0 && $('#search').length > 0){
        $('#search').val(searchFor.text());
        $('.filter-clear').click();
        $('#search').goTo();
      }else{
        window.location.href="asortyment.html";
      }
    });
    if(basketBodyTether!==undefined){
      basketBodyTether.position();
    }
  }
}
(function($) {
    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'fast');
        return this; // for chaining...
    }
})(jQuery);

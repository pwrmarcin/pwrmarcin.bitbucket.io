function initMap() {
	var uluru = {lat: 50.772547, lng: 16.267515};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 18,
	  center: uluru
	});
	var marker = new google.maps.Marker({
	  position: uluru,
	  map: map
	});
}
$(document).ready(function() {
	initImg();
});
function initImg(){
	$('.img-thumbnail').mouseover(function(){
    var index = $(this).index();
		$('.img-fluid').attr("src",$(this).attr("src"));
		gtag('event', 'FotoPlac', {
		  'event_category': 'Hover',
		  'event_label': 'foto'+index,
		  'value': index
		});
	});
}

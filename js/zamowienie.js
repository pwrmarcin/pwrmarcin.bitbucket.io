var order;
function initMap() {
	var uluru = { lat: 50.772547, lng: 16.267515 };
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 18,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
}
$(document).ready(function() {
  initLoad();
});
function initLoad(){
  var postData = { v: getParameterByName('v') };
  $('.basketBody').show();
  showLoading('.basketBody');
  $.post("http://35.189.206.41/Home/Zamowienie", postData, function( data ) {
    updateOrderSummary(data);
    $('.loading').hide();
  }, "json");
}
function showLoading(className){
  $('.loading').show();
  new Tether({
      element: '.loading',
      target: className,
      attachment: 'middle center',
      targetAttachment: 'middle center',
      offset: '-64px 0'
    });
}
function updateOrderSummary(zamowienie){
  if(zamowienie !== undefined && zamowienie !== null){
    var template = "<table class=\"table-art-basket\" style=\"width: 100%;\">" +
            "<thead><tr><th>Kod</th><th>Nazwa towaru</th><th>Miara</th><th>Pakowany</th><th>Cena z VAT</th><th>Ilość</th><th>Suma</th></tr></thead>" +
          "<tbody>{{#Records}}<tr><td>{{Kod}}</td><td>{{Name}}</td><td>{{Miara}}</td><td>{{Opak}}</td><td>{{Price}}</td><td>{{Count}}</td><td>{{Sum}}</td></tr>{{/Records}}</tbody>" +
          "</table>";
    var html = $(Mustache.to_html(template, zamowienie));
    $('.basket-sum').html(zamowienie.TotalSum);
    $('.basket-count').html(zamowienie.ItemsCount);
    $('.basket-copy').html(html);
    $('.basket').css({width:'100%',height:'100%'});
    $('.top-basket').css({width:'100%',height:'100%'});
  }
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

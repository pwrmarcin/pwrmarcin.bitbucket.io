function initMap() {
	var uluru = {lat: 50.772547, lng: 16.267515};
	var map = new google.maps.Map(document.getElementById('map'), {
	  zoom: 18,
	  center: uluru
	});
	var marker = new google.maps.Marker({
	  position: uluru,
	  map: map
	});
	var uluru2 = {lat: 50.781449, lng: 16.266371};
	var map2 = new google.maps.Map(document.getElementById('map2'), {
	  zoom: 18,
	  center: uluru2
	});
	var marker2 = new google.maps.Marker({
	  position: uluru2,
	  map: map2
	});
}
$(document).ready(function() {
	initImg();
});
function initImg(){
	$('.img-thumbnail').mouseover(function() {
    var index = $(this).index();
    console.log(index);
		$('.img-fluid').attr("src",$(this).attr("src"));
		gtag('event', 'FotoHala', {
		  'event_category': 'Hover',
		  'event_label': 'foto'+index,
		  'value': index
		});
	});
}
